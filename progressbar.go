package main

import (
	"fmt"
	"math"
	"strings"
	"time"
)

func main() {
	items := make([]int, 49)
	l := len(items)
	printprogressbar(0, l)
	for i := 0; i < l; i++ {
		printprogressbar(i+1, l)
	}
}

func printprogressbar(iteration int, total int) {
	length := 50
	fill := '\u2588'
	percent := fmt.Sprintf("%v", int(math.Floor(float64(iteration)/float64(total)*100)))
	filled := int(math.Floor(float64(float64(length*iteration) * (1 / float64(total)))))
	bar := func(fill rune, filled int) string {
		wrap := []string{fmt.Sprintf("%c", '\u2588')}
		for i := 0; i < filled; i++ {
			wrap = append(wrap, fmt.Sprintf("%c", '\u2588'))
		}
		bar := strings.Join(wrap, "")
		return bar
	}(fill, filled)
	strip := func(lengt int) string {
		var left string
		for i := 1; i < lengt; i++ {
			left = left + "-"
		}
		return left
	}(length - filled)
	time.Sleep(100 * time.Millisecond)
	fmt.Printf("\rProgress | %v%v | %v %v Complete", bar, strip, percent, "%")
	if iteration == total {
		fmt.Println()
	}
}
